<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

<br /> ユーザー新規登録<br /> <br />
<form action="signup" method="post">

		<table class="table1" border=1>
		<tr><th>ログインID</th><td align="center">
		<input name="login_id" id="login_id" /></td></tr>

		<tr><th>パスワード</th><td align="center">
		<input name="password1"	type="password" id="password" /></td></tr>

		<tr><th>パスワード（確認用）</th><td align="center">
		<input name="password2" type="password" id="password" /></td></tr>

		<tr><th>ユーザー名</th><td align="center">
		<input name="name" value="${editUser.name}" id="name"></td></tr>

		<tr><th>支店</th><td align="left">
                <select name="branch_id">
                    <option value="1">本社</option>
                    <option value="2">支店A</option>
                    <option value="3">支店B</option>
                    <option value="4">支店C</option>
                </select></td>
        </tr>

		<tr><th>部署・役職</th><td align="left">
		         <select name="department_id">
                    <option value="1">人事総務部</option>
                    <option value="2">情報管理部</option>
                    <option value="3">支店長</option>
                    <option value="4">社員</option>
                </select></td>
        </tr>
        </table><br />
		<input type="submit" value="登録" /> <br /> <br />
</form>
		<a href="management">戻る</a>
		<div class="copyright">Copyright(c)Fukushima Momoko</div>
	</div>
</body>
</html>
