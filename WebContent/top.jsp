<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
<style>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</style>
</head>

<body>
	<div class="header">
		<div class="main-contents">
			<%-- ↓ログインしてないユーザー empty =空の --%>
			<!-- エラーメッセージを表示させる-->
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
			</c:if>
		</div>

		<%-- ↓ログインしているユーザー  not empty =空ではない --%>
			<c:out value="${error}" /><br />
			<c:remove var="error" scope="session"/>

		<%--リンクの遷移先のURLはsettings  --%>
		<a href="management">ユーザー管理</a>

		<%--リンクの遷移先のURLは newMessage  --%>
		<a href="newMessage">新規投稿</a>

		<%--リンクの遷移先のURLはlogout  --%>
		<a href="logout">ログアウト</a><br />
		<br />

		<%-- ログインユーザーの情報を表示させるためのコード --%>
		<div class="user">
			ログイン中：
			<c:out value="${loginUser.name}" /><br /><br /><br />
		</div>
	</div>

    <form method="get" action="./" target="_self">
    <!-- _self = 現在のウィンドウに開く-->

    ↓カテゴリー検索<br />
    <input type="text" name="search" size="31" maxlength="255">
    <!-- name = servlet内の名前と一致させる-->
    <input type="hidden" name="search" value="${category.category}">
    <input type="submit" name="search" value="検索"><br />


    ↓投稿日（期間指定）<br />
    <input type="date" name="start_date" value="" />

    ～<input type="date" name="end_date" value="" />

    <input type="submit" name="date_search" value="絞込み"><br /><br />

    </form><br />



	<%-- 投稿を表示するためのコード --%>
	<div class="messages">
		<%-- ↓c:forEachでfor文で回す --%>
		<%-- var = ${}内の名前と繋がる（同じ） --%>

		<c:forEach items="${messages}" var="message">


			<div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; border-radius: 10px;">
				<div class="message"><br />


						<%-- <c:out> =表示させる --%>
					<c:forEach items="${users}" var="user">
						<c:if test="${user.id == message.userId}">
							<c:out value="${user.name}" />さん -
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
						</c:if>
					</c:forEach><br /><br />

                    <div class="category">
						カテゴリー：
						<c:out value="${message.category}" />
						<br />
					</div>
				    <div class="subject">
						件名：
						<c:out value="${message.subject}" />
						<br />
				    </div>

					<div class="text">
						<c:forEach items="${message.text}" var="text">
						  <c:out value="${text}" /><br />
					    </c:forEach>
						<br />
					</div>

					<form action="delete" method="post">
						<input type="hidden" name="message_id" value="${message.id}">
						<input type="submit" value="投稿削除"><br />
						<br />
					</form>
				</div>


				<!-- コメント表示 -->
				<div class="comments">
					<c:forEach items="${comments}" var="comment">
						<div class="comment">


							<%-- <c:if test=>でifの条件指定 投稿IDとコメントIDが一致していたら表示する--%>
							<c:if test="${message.id == comment.messageId}"><br />

                               <div class="comment-user">
									<!-- items = 繰り返し処理の対象となる集合（配列、Mapなど）を指定する。 -->
									<c:forEach items="${users}" var="user">
										<c:if test="${user.id == comment.userId}">
							        <c:out value="${user.name}" />さん -
									<fmt:formatDate value="${comment.createdDate}"
										pattern="yyyy/MM/dd HH:mm:ss" />
							            </c:if>
									</c:forEach>
								</div>



                            <div class="comment">
							<div style="padding: 10px; margin-bottom: 10px; border: 1px solid #333333;
							      border-radius: 10px; background-color: #ffff99;">

							      <c:forEach items="${comment.comment}" var="commentText">
									<c:out value="${commentText}" /><br />
								  </c:forEach>
							 </div>
							</div>



							<form action="deleteComment" method="post">
								    <input type="hidden" name="comment_id" value="${comment.id}">
									<input type="submit" value="↑コメント削除">
								    </form>
							</c:if>
						</div>
					</c:forEach>

					<!-- ↓コメント入力フォーム -->
					<div class="form-area">
						<form action="comment" method="post">
							<input type="hidden" name="message_id" value="${message.id}"><br />
							↑コメント（500文字まで） <br />
							<textarea name="comment" cols="100" rows="3" wrap="hard"></textarea>
							<br /> <input type="submit" value="コメントする"><br />
							<br />
						</form>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
	<br />
	<a href="./">戻る</a>
	<br />
	<div class="copyright">Copyright(c)Fukushima Momoko</div>
</body>
</html>
