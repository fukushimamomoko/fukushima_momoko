<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset="UTF-8">
<title>ユーザー管理</title>
</head>
<body>
	ユーザー管理<br />	<br />
	<div class="main-contents">

		<%--リンクの遷移先のURLはsignup  --%>
		<a href="signup">ユーザー新規登録</a><br /> <br />


		<table class="table1" border=1 >
			<tr>
				<th>ユーザー名</th>
				<th>支店</th>
				<th>部署･役職</th>
				<th>編集</th>
				<th>停止/復活</th>


				<c:forEach items="${users}" var="user">
					<!-- ↓中身はひとつひとつc:outする-->
					<tr>
						<!-- beanの中のget以降の名前の頭文字を小文字にした名前 -->
						<td align="center"><c:out value="${user.name}" /></td>
						<td align="center"><c:out value="${user.branchName}" /></td>
						<td align="center"><c:out value="${user.departmentName}" /></td>


						<%--リンクの遷移先のURLは  --%>
						<td><a href="settings?id=${user.id}">編集</a>
						<!-- INPUT = <form>タグで作成したフォームの中でテキスト入力欄やボタンなどの部品を作成する要素 -->

						<td  align="center">
						<input name="id" value="${editUser.id}" id="id" type="hidden"/>

							<c:if test="${user.stop == 1}">
							<form action="management" method="post">
							    <input type="hidden" name="id" value="${user.id}">
							    <input type="hidden" name="userStop" value="${user.stop}">
								<input type="submit" value="停止" onclick="return disp();">
							</form>
							</c:if>

							<c:if test="${user.stop == 0}">
							<form action="management" method="post">
							    <input type="hidden" name="id" value="${user.id}">
							    <input type="hidden" name="userStop" value="${user.stop}">
							    <!-- value = 何の情報を送るか -->
								<input type="submit" value="復活" onclick="return revival();">
							</form>
							</c:if>


							<script>
							function disp(){
								// 「OK」時の処理開始 ＋ 確認ダイアログの表示
								if(window.confirm('ユーザーを停止しますか？')){
									window.location.href = "/management"; // /managament へジャンプ
								}
								// 「OK」時の処理終了

								// 「キャンセル」時の処理開始
								else{
									window.alert('キャンセルされました'); // 警告ダイアログを表示
									return false;
								}
								// 「キャンセル」時の処理終了
							}
							</script>

							<script>
							function revival(){

								// 「OK」時の処理開始 ＋ 確認ダイアログの表示
								if(window.confirm('ユーザーを復活させますか？')){
									window.location.href = "/management"; // /managament へジャンプ
								}
								// 「OK」時の処理終了

								// 「キャンセル」時の処理開始
								else{
									window.alert('キャンセルされました'); // 警告ダイアログを表示
									return false;
								}
								// 「キャンセル」時の処理終了
							}
							</script>

					</tr>
				</c:forEach>
		    </table>


		<br /> <a href="./">戻る</a>

		<div class="copyright">Copyright(c)Fukushima Momoko</div>
	</div>
</body>
</html>