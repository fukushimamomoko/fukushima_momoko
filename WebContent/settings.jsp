<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->
</head>
<body>
	ユーザー編集<br /> <br />


	<div class="form-area">


        <c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="settings">
						<li><c:out value="${settings}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="settings" method="post">
		<!-- action =フォームの送信ボタンを押して送信されるデータの送信先を指定する -->
		<table class="table1" border="1">


				<tr>
					<td align="center">ユーザー名</td>
					<td align="left">
					<input type="text" name="name" size="16" value="${editUser.name}" >
					</td>
				</tr>
				<tr>
					<td align="center">パスワード</td>
					<td align="left"><input type="password" name="password1"
						size="16" ></td>
				</tr>
				<tr>
					<td align="center">パスワード(確認用)</td>
					<td align="left"><input type="password" name="password2"
						size="16" ></td>
				</tr>
				<tr>
					<td align="center">ログインID</td>
					<td align="left">
					<input type="text" name="login_id" size="16" value="${editUser.loginId}">
					</td>
				</tr>

				<tr>
					<td align="center">支店</td>
					<td align="left">
				<select name="branch_id" id="branch_id">
                    <option value="1">本社</option>
                    <option value="2">支店A</option>
                    <option value="3">支店B</option>
                    <option value="4">支店C</option>
                </select>
                	</td>
				</tr>

				<tr>
					<td align="center">部署･役職</td>
					<td align="left">
					<input type="hidden">
				<select name="department_id">
                    <option value="1">人事総務部</option>
                    <option value="2">情報管理部</option>
                    <option value="3">支店長</option>
                    <option value="4">社員</option>
                </select>
					</td>
				</tr>

		</table>



		<br />
		<input type="hidden" name="id" value="${editUser.id}">
		<input type="submit" value="変更する">
		</form> <br /> <br />

		<a href="management">戻る</a>

		<div class="copyright">Copyright(c)Fukushima Momoko</div>
	</div>
</body>
</html>