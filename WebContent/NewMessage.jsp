<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>新規投稿画面</title>
</head>

<!-- <div class="header">

</div> -->

<body>
新規投稿
<br />
<br />
            <!-- エラーメッセージの表示 -->
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if><br />

<div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; border-radius: 10px;">

	<div class="form-area">
		<form action="newMessage" method="post">
			カテゴリー<br />
			<input maxlength='10'name="category" >

			<br /> 件名<br />
			<input maxlength='30' name="subject" >

			<br /> 本文（1000文字まで） <br />
			<textarea name="text" cols="100" rows="5"></textarea>
			<br />

			<!-- ↓新規投稿ボタン -->
			<input type="submit" value="投稿する">

		</form>
	</div>
</div>
	<br />
	<br />
	<a href="./">戻る</a>


	<div class="copyright">Copyright(c)Fukushima Momoko</div>
	<br />
</body>
</html>



