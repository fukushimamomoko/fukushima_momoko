package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

//データベースの表結合から値を取得するためのDAO
public class UserCommentDao {

	//Connection =データベースとの接続
	public List<UserComment> getUserComments(Connection connection, int num) {

		//PreparedStatement =データベース接続処理
		PreparedStatement ps = null;

		try {
			//StringBuilder =String型みたいなもの
			//StringBuilder =必要最小限の保存スペースだけを使い文字列を連結することができる
			StringBuilder sql = new StringBuilder();

			//大量に文字列を繋げていく場合にappendメソッドを使う
			//選択する
			sql.append("SELECT ");
			//カンマの後にスペースを空けないとくっつく
			//カラム1 AS 別名1,(表結合の際に必要)
			sql.append("comments.id as id, ");
			sql.append("comments.message_id as message_id, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.comment as comment, ");
			sql.append("comments.created_date as created_date, ");
			//sql.append("messages.updated_date as updated_date, ");
			//最後はカンマをつけない（区切る必要がないから）
			sql.append("users.name as name, ");
			sql.append("comments.user_id as user_id ");
			//append = 文字列を追加する
			//下記のテーブルから
			sql.append("FROM comments ");
			//↑ここまでセット

			//SELECT 文と INNER JOIN 句を組み合わせることで
			//2つのテーブルを内部結合させてデータを取得する
			sql.append("INNER JOIN users ");

			//messagesテーブルのuser_idにusersのidを連結
			//users.id = 何番目のユーザーか  messages.user_id = だれの投稿か
			//二つを同じものに（内部連結）
			sql.append("ON comments.user_id = users.id ");

			//番号順（降順）に表示する
			//(+)演算子を使用して文字列を連結
			sql.append("ORDER BY created_date DESC limit " + num);

			System.out.println(sql.toString());

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserComment> ret = toUserCommentList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserComment> getComments(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY comments.created_date ASC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int message_id = rs.getInt("message_id");
				int user_id = rs.getInt("user_id");
				String comment = rs.getString("comment");
				Timestamp createdDate = rs.getTimestamp("created_date");
				String name = rs.getString("name");

				//UserCommentのbeanから
				UserComment message = new UserComment();
				message.setId(id);
				message.setMessageId(message_id);
				message.setUserId(user_id);
				message.setComment(comment);
				message.setCreatedDate(createdDate);
				message.setName(name);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
