package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments( ");//テーブルの名前
			sql.append("id");//データベースの中のID（番号）
			sql.append(", message_id");
			sql.append(", comment");//コメント本文
			sql.append(", user_id");//ユーザーID
			sql.append(", created_date");//投稿時間
			sql.append(") VALUES (");//値をすべて取り出す
			sql.append("?"); //id
			sql.append(", ?"); //message_id
			sql.append(", ?"); //comment
			sql.append(", ?");//user_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(")");//VALUES

			ps = connection.prepareStatement(sql.toString());

			//?に数値を入れる。
			ps.setInt(1, comment.getId());
			ps.setInt(2, comment.getMessageId());
			ps.setString(3, comment.getCommentStr());
			ps.setInt(4, comment.getUserId());

			//前処理済みのSQL文（検索系SQL以外のSQL文）を実行し，更新行数を返却
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Comment> toCommentList(ResultSet rs) throws SQLException {

		List<Comment> ret = new ArrayList<>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int messageId = rs.getInt("message_id");
				String comment = rs.getString("comment");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");
				int userId = rs.getInt("user_id");

				Comment comments = new Comment();
				comments.setId(id);
				comments.setMessageId(messageId);
				comments.setComment(comment);
				comments.setCreatedDate(createdDate);
				comments.setUpdatedDate(updatedDate);
				comments.setUserId(userId);

				ret.add(comments);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<Comment> getComments(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * ");
			sql.append("FROM comments ");
			sql.append("ORDER BY created_date ASC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Comment> ret = toCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int commentId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, commentId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
