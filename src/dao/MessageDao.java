package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("id");//
			sql.append(", category");//カテゴリー
			sql.append(", subject");//件名
			sql.append(", text");//本文
			sql.append(", user_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");//値をすべて取り出す
			sql.append("?"); //id
			sql.append(", ?"); // category
			sql.append(", ?");//subject
			sql.append(", ?"); // text
			sql.append(", ?");//user_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");//VALUES

			ps = connection.prepareStatement(sql.toString());

			//?に数値を入れる。
			ps.setInt(1, message.getId());
			ps.setString(2, message.getCategory());
			ps.setString(3, message.getSubject());
			ps.setString(4, message.getTextStr());
			ps.setInt(5, message.getUserId());

			//前処理済みのSQL文（検索系SQL以外のSQL文）を実行し，更新行数を返却
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Message> toMessageList(ResultSet rs) throws SQLException {

		List<Message> ret = new ArrayList<>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");
				int userId = rs.getInt("user_id");

				Message message = new Message();
				message.setId(id);
				message.setSubject(subject);
				message.setText(text);
				message.setCategory(category);
				message.setCreatedDate(createdDate);
				message.setUpdatedDate(updatedDate);
				message.setUserId(userId);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<Message> getMessages(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT *");
			sql.append(" FROM messages");
			//sql.append(" WHERE ");
			sql.append(" ORDER BY messages.created_date DESC");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			List<Message> ret = toMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Message> getMessages(Connection connection, String word, String start, String end) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();//投稿日（期間指定）で表示の絞り込み
			sql.append("SELECT *");
			sql.append(" FROM messages");

			sql.append(" INNER JOIN users");
			sql.append(" ON users.id = messages.user_id ");

			sql.append(" WHERE category");
			sql.append(" LIKE ?");
			sql.append(" AND messages.created_date");
			sql.append(" BETWEEN ?");
			sql.append(" AND ? ");
			sql.append(" ORDER BY messages.created_date DESC");

			ps = connection.prepareStatement(sql.toString());
			// % = 任意の0文字以上の文字列
			ps.setString(1, "%" + word + "%");
			if (StringUtils.isEmpty(start)) {
				start = "2001-01-01";
			}
			ps.setString(2, start + " 00:00:00");
			if (StringUtils.isEmpty(end)) {
				end = "2205-12-31";
			}
			ps.setString(3, end + " 23:59:59");
			ResultSet rs = ps.executeQuery();
			List<Message> ret = toMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int messageId) {

		PreparedStatement psMessage = null;
		PreparedStatement psComment = null;
		try {
			// 投稿の削除
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM messages");
			sql.append(" WHERE ");
			sql.append(" id = ?");

			psMessage = connection.prepareStatement(sql.toString());

			psMessage.setInt(1, messageId);
			psMessage.executeUpdate();

			// 投稿にひもづいたコメントの削除
			sql = new StringBuilder();
			sql.append("DELETE FROM comments");
			sql.append(" WHERE");
			sql.append(" message_id = ?");

			psComment = connection.prepareStatement(sql.toString());

			psComment.setInt(1, messageId);
			psComment.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(psMessage);
			close(psComment);
		}
	}

	public void deleteComment(Connection connection, int commentId) {

		PreparedStatement psComment = null;
		try {
			//コメント削除
			StringBuilder sql = new StringBuilder();

			sql = new StringBuilder();
			sql.append("DELETE FROM comments");
			sql.append(" WHERE");
			sql.append(" id = ?");

			psComment = connection.prepareStatement(sql.toString());

			psComment.setInt(1, commentId);
			psComment.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(psComment);
		}
	}
}