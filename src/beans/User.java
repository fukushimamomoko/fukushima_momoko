
package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;//データベース上のID
	private String login_id;//ログイン時に使用するID（ログインしているユーザーのID）
	private String name;
	private String password;
	private String checkPassword;
	private int department_id;//役職ID
	private int branch_id;//支店ID
	private String department_name;//役職名
	private String branch_name;//支店名
	private int stop;
	private Date createdDate;
	private Date updatedDate;


	//beansの中
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return login_id;
	}

	public void setLoginId(String login_id) {
		this.login_id = login_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getDepartmentId() {
		return department_id;
	}

	public void setDepartmentId(int department_id) {
		this.department_id = department_id;
	}

	public int getBranchId() {
		return branch_id;
	}

	public void setBranchId(int branch_id) {
		this.branch_id = branch_id;
	}

	public String getBranchName() {
		return branch_name;
	}

	public void setBranchName(String branch_name) {
		this.branch_name = branch_name;
	}

	public String getDepartmentName() {
		return department_name;
	}

	public void setDepartmentName(String department_name) {
		this.department_name = department_name;
	}

	public int getStop() {
		return stop;
	}
	public void setStop(int isStop) {
		this.stop = isStop;
	}

	public String getCheckPassword() {
		return checkPassword;
	}

	public void setCheckPassword(String checkPassword) {
		this.checkPassword = checkPassword;
	}

}
