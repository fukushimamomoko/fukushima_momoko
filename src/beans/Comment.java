package beans;
//コメントそのもの

import java.io.Serializable;//beanの永続化（必要に応じて保存したり復元したり）
import java.util.Date;

public class Comment implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;//コメントそのものを特定するID
	private int message_id;//投稿を特定するID（どの投稿に対してのコメントなのか）
	private int user_id;//コメントした人のID
	private String comment;//コメント本文
	private Date createdDate;//コメントした日付
	private Date updatedDate;
	private String name;

	//beansの中
	//メッセージの表示機能
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMessageId() {
		return message_id;
	}

	public void setMessageId(int message_id) {
		this.message_id = message_id;
	}

	public int getUserId() {
		return user_id;
	}

	public void setUserId(int user_id) {
		this.user_id = user_id;
	}

	public String[] getComment() {
		String[] getComment = comment.split("\r\n");
		return getComment;
	}

	public String getCommentStr() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
