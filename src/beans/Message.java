package beans;

//投稿内容そのもの

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;//投稿そのものを特定するID
	private String subject;//件名
	private String text;//本文
	private String category;//カテゴリー
	private Date createdDate;
	private Date updatedDate;
	private int userId;//投稿者のid

	//つぶやきメッセージを表すMessageというBeanを
    //MessageService#register()の引数として渡し、メッセージの登録を行う

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String[] getText() {
		String[] getText = text.split("\r\n");
		return getText;
	}

	public String getTextStr() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
