package service;


import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.UserComment;
import dao.CommentDao;
import dao.UserCommentDao;


//servletからメッセージ一覧を取得する
public class CommentService {
	//register =登録する
	//Comment =bean.Comment ここにcommentの内容を登録する
	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			//コメントに変更
			CommentDao commentDao = new CommentDao();

			//対象文字列.insert = 挿入する(追加位置,追加対象 )
			//コメントに変更
			commentDao.insert(connection, comment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	public List<UserComment> getComments() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserCommentDao usercommentDao = new UserCommentDao();
			List<UserComment> ret = usercommentDao.getComments(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public void deleteComment(int commentId) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.delete(connection, commentId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}

