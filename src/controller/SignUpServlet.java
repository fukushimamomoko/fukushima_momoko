package controller;//ユーザー新規登録

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			//変数userに下の情報をまとめて入れる
			User user = new User();
			//jspで入力された情報をbeanのlogin_idに入れる
			user.setLoginId(request.getParameter("login_id"));
			user.setName(request.getParameter("name"));
			user.setPassword(request.getParameter("password1"));
			user.setCheckPassword(request.getParameter("password2"));
			user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
			user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));

			new UserService().register(user);//登録する

			response.sendRedirect("login");//成功したらログイン画面に遷移する
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("signup");//エラーになれば編集画面のまま
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String login_id = request.getParameter("login_id");
		String name = request.getParameter("name");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");

		if (StringUtils.isEmpty(name) == true) {
			messages.add("ユーザー名を入力してください");
		}
		if (10 < name.length()) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}
		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("ログインIDを入力してください");
		}
		if (login_id == "^\\d{6,20}$" == false) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		}
		if ((login_id == "^[0-9a-zA-Z]+$") == false) {
			messages.add("ログインIDは半角英数字で入力してください");
		}
		if (StringUtils.isEmpty(password1) || (password2) == null) {
			messages.add("パスワードを入力してください");
		} else {

			if (StringUtils.isEmpty(password1) == true) {
				messages.add("パスワードを入力してください");
			}
			if (StringUtils.isEmpty(password2) == true) {
				messages.add("確認のためもう一度パスワードを入力してください");
			}
			if (password1.equals(password2) != true) {
				messages.add("パスワードが一致しません");
			}
			if ((password1 == "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!-/:-@[-`{-~])[!-~]$") == true) {
				messages.add("パスワードは記号を含む全ての半角文字で入力してください");
			}
			if (password1.length() <= 6 && password1.length() >= 20) {
				messages.add("パスワードは6文字以上20文字以下で入力してください");
			}
			if (password2.length() <= 6 && password2.length() >= 20) {
				messages.add("パスワードは6文字以上20文字以下で入力してください");
			}
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
