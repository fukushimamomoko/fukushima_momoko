package controller;//ユーザー管理画面

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, //入力値は無かったから
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User loginUser = new User();
		loginUser = (User) session.getAttribute("loginUser");

		//ログインユーザーの支店が001（本社）以外の場合
		//部署が001以外の場合
		if (!(loginUser.getBranchId() == 1 && loginUser.getDepartmentId() == 1)) {

			List<String> error = new ArrayList<String>();
			error.add("該当権限を持っていないので開けません");
			session.setAttribute("error", error);
			response.sendRedirect("./");//top画面に跳ぶ

		} else {
			//値を保存
			List<User> users = new UserService().getUsers();
			request.setAttribute("users", users);
			request.getRequestDispatcher("/management.jsp").forward(request, response);//配送の支持を行うクラス

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		if (request.getParameter("userStop").equals("1")) {//文字列同士を比較する equals
			new UserService().stop(Integer.parseInt(request.getParameter("id")));
		} else {
			new UserService().revival(Integer.parseInt(request.getParameter("id")));
		}

		response.sendRedirect("management");

	}

}
