package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import beans.UserComment;
import service.CommentService;
import service.MessageService;
import service.UserService;

//メッセージを取得し、リクエストにメッセージをセットするコード

@WebServlet(urlPatterns = { "/index.jsp" }) //←indexは変えない
//extends =メンバ変数やメンバメソッドをそのまま使う
public class TopServlet extends HttpServlet {
	//クラス定数を宣言↓はlong型
	private static final long serialVersionUID = 1L;

	@Override
	//Webブラウザは、Webサーバに対して要求(request)を出す
	protected void doGet(HttpServletRequest request,
			//Webサーバは、Webブラウザからの要求を処理して、応答(response)を返す
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		User loginUser = new User();
		loginUser = (User) session.getAttribute("loginUser");

		if (loginUser == null) {//ログインユーザーがnullの場合
			response.sendRedirect("login");//ログイン画面に跳ぶ

		} else {//ユーザーがログインした状態なら

			//値を保存
			List<User> users = new UserService().getUsers();

			//検索フォーム、日付け検索すべての中身が空の場合
			if (StringUtils.isEmpty(request.getParameter("search"))
					&& StringUtils.isEmpty(request.getParameter("start_date"))
					&& StringUtils.isEmpty(request.getParameter("end_date"))) {
				List<Message> messages = new MessageService().getMessages();
				request.setAttribute("messages", messages);//すべて表示する

			} else {
				List<Message> messages = new MessageService().getMessages(request.getParameter("search"),
						request.getParameter("start_date"),
						request.getParameter("end_date"));
				request.setAttribute("messages", messages);
			}

			List<UserComment> comments = new CommentService().getComments();

			request.setAttribute("users", users);
			request.setAttribute("comments", comments);
			request.getRequestDispatcher("/top.jsp").forward(request, response);//配送の支持を行うクラス
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		User loginUser = new User();
		loginUser = (User) session.getAttribute("loginUser");//サーバーに保存されているセッションオブジェクトを取り出して読み込む

		if (loginUser == null) {
			response.sendRedirect("login");
		} else {

			request.setCharacterEncoding("UTF-8");

			List<Message> messages = new MessageService().getMessages();
			List<UserComment> comments = new CommentService().getComments();

			request.setAttribute("messages", messages);//投稿
			request.setAttribute("comments", comments);//コメント
			request.getRequestDispatcher("/top.jsp").forward(request, response);
		}
	}
}
