package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> comments = new ArrayList<String>();

		//isValid =メソッドでセッションが有効であるとみなしているかどうか判定する
		if (isValid(request, (comments)) == true) {

			//引数loginUserで指定したオブジェクトを取得する。
			User user = (User) session.getAttribute("loginUser");

			Comment comment = new Comment();
			//↓getParameterは必ずSring型で返ってくる。intで返したい場合は変換する
			comment.setUserId(user.getId());//コメントした人のID

			//投稿を特定するID（どの投稿に対するコメントか）
			//top.jspのname="message_id"←の名前と一致させる(連携しているため)
			comment.setMessageId(Integer.parseInt(request.getParameter("message_id")));
			comment.setComment(request.getParameter("comment"));
			//register =登録する
			new CommentService().register(comment);

			response.sendRedirect("./");//トップ画面に戻る
		} else {
			session.setAttribute("commentError", comments);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> comments) {

		//エラーメッセージの表示
		String comment = request.getParameter("comment");

		//isEmpty対象の文字列が空文字列なのかを判定することができる
		if (StringUtils.isEmpty(comment) == true) {
			comments.add("コメントを入力してください");
		}
		if (500 < comment.length()) {
			comments.add("500文字以下で入力してください");
		}

		//要素が0ならtrueを返す
		if ((comments.size()) == 0) {
			return true;
		} else {
			return false;
		}
	}

}
