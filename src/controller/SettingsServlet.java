package controller;//ユーザー編集画面

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override //このページの中身
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("id"));//URLの ?=...
		User editUser = new UserService().getUser(id);//1つだけ取って来たいからList型ではなくUser型
		request.setAttribute("editUser", editUser);

		//移動先("settings.jsp")
		request.getRequestDispatcher("settings.jsp").forward(request, response);
	}

	@Override //このページで何をするか
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		//エラーメッセージを表示する変数
		List<String> settings = new ArrayList<String>();

		HttpSession session = request.getSession();
		User user = new User();
		user.setLoginId((request.getParameter("login_id")));
		user.setPassword(request.getParameter("password1"));
		user.setName(request.getParameter("name"));
		user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));
		user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));//支店名
		user.setId(Integer.parseInt(request.getParameter("id")));

		if (isValid(request, settings) == true) {

			try {
				new UserService().update(user);
				response.sendRedirect("management");//編集画面に戻る
			} catch (NoRowsUpdatedRuntimeException e) {
				//settings.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", settings);
				request.setAttribute("editUser", user);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}
		} else {
			session.setAttribute("errorMessages", settings);
			request.setAttribute("editUser", user);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	//エラー時の表示コメント
	private boolean isValid(HttpServletRequest request, List<String> settings) {

		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");//passwordが一致しているか確認

		if (StringUtils.isEmpty(name) == true) {
			settings.add("ユーザー名を入力してください");
		}
		if (10 < name.length()) {
			settings.add("ユーザー名は10文字以下で入力してください");
		}
		if (StringUtils.isEmpty(login_id) == true) {
			settings.add("ログインIDを入力してください");
		}
		if (login_id.matches("^.{6,20}$")== false) {
			settings.add("ログインIDは6文字以上20文字以下で入力してください");
		}
		if (login_id.matches("^[0-9a-zA-Z]+$") == false) {
			settings.add("ログインIDは半角英数字で入力してください");
		}

		if (!(StringUtils.isEmpty(password1) || StringUtils.isEmpty(password2))) {//1と2どちらかが空白でなければ

			if (StringUtils.isEmpty(password1) == true) {
				settings.add("パスワードを入力してください");
			}
			if (StringUtils.isEmpty(password2) == true) {
				settings.add("確認のためもう一度パスワードを入力してください");
			}
			if (password1.equals(password2) != true) {
				settings.add("パスワードが一致しません");
			}
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (settings.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
