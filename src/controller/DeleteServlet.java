package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.DeleteService;


@WebServlet(urlPatterns = {"/delete"})
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//削除する投稿IDを受け取る
		new DeleteService().deleteMessage(Integer.parseInt(request.getParameter("message_id")));

		response.sendRedirect("./");//トップ画面に戻る
	}

}
