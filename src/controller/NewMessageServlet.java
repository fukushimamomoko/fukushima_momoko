package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		//jspの名前に合わせる
		request.getRequestDispatcher("NewMessage.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, (messages)) == true) {

			//引数loginUserで指定したオブジェクトを取得する。
			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setCategory(request.getParameter("category"));
			message.setSubject(request.getParameter("subject"));
			message.setText(request.getParameter("text"));
			message.setUserId(user.getId());

			//register =登録する
			new MessageService().newMessage(message);

			response.sendRedirect("./");//正しく投稿できたらトップ画面に戻る
		} else {
			session.setAttribute("errorMessages", messages);//エラーメッセージが表示され、
			response.sendRedirect("newMessage");//エラーになれば投稿画面のまま
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String subject = request.getParameter("subject");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		//エラーメッセージの表示
		//isEmpty =対象の文字列が空文字列なのかを判定することができる

		if (StringUtils.isEmpty(subject) == true) {
			messages.add("件名を入力してください");
		}
		if (StringUtils.isEmpty(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if (StringUtils.isEmpty(text) == true) {
			messages.add("本文を入力してください");
		}
		if (30 < subject.length()) {
			messages.add("件名は30文字以下で入力してください");
		}
		if (10 < category.length()) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}
		if (1000 < text.length()) {
			messages.add("本文は1000文字以下で入力してください");
		}

		//要素が０ならtrueを返す
		if ((messages.size()) == 0) {
			return true;
		} else {
			return false;
		}
	}

}
